package com.onuryardimci.codility.lesson1;

class BinaryGap {

    public static void main(String args[]) {
        BinaryGap binaryGap = new BinaryGap();

        System.out.println(binaryGap.solution(9));
        System.out.println(binaryGap.solution(529));
        System.out.println(binaryGap.solution(20));
        System.out.println(binaryGap.solution(15));
        System.out.println(binaryGap.solution(1041));
    }

    public int solution(int N) {
        int mod = N % 2;
        while (N != 1 && mod == 0) {
            N = N / 2;
            mod = N % 2;
        }
        int maxZeroStreak = 0;
        int zeroStreak = 0;
        while (N != 1) {
            int modulus = N % 2;
            if (modulus == 0) {
                zeroStreak++;
            } else {
                maxZeroStreak = Math.max(maxZeroStreak, zeroStreak);
                zeroStreak = 0;
                N--;
            }
            N = N / 2;

        }
        return Math.max(maxZeroStreak, zeroStreak);
    }
}

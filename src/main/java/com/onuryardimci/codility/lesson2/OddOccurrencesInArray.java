package com.onuryardimci.codility.lesson2;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by onury.
 */
public class OddOccurrencesInArray {

    public static void main(String[] args) {
        OddOccurrencesInArray oddOccurrencesInArray = new OddOccurrencesInArray();

        System.out.println(oddOccurrencesInArray.solution(new int[]{9, 3, 9, 3, 9, 7, 9}));
    }

    public int solution(int[] A) {
        Set<Integer> existingValues = new HashSet<Integer>();
        for (int i: A) {
            addOrRemoveValue(existingValues, i);
        }
        return existingValues.iterator().next();
    }

    private void addOrRemoveValue(Set<Integer> existingValues, int i) {
        if (existingValues.contains(i)) {
            existingValues.remove(i);
        } else {
            existingValues.add(i);
        }
    }
}

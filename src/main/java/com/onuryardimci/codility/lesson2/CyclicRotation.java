package com.onuryardimci.codility.lesson2;

import java.util.Arrays;

/**
 * Created by onury.
 */
public class CyclicRotation {

    public static void main(String[] args) {
        CyclicRotation cyclicRotation = new CyclicRotation();

        System.out.println(Arrays.toString(cyclicRotation.solution(new int[]{3, 8, 9, 7, 6}, 3)));
        System.out.println(Arrays.toString(cyclicRotation.solution(new int[]{0, 0, 0}, 1)));
        System.out.println(Arrays.toString(cyclicRotation.solution(new int[]{1, 2, 3, 4}, 4)));
    }

    public int[] solution(int[] A, int K) {

        int length = A.length;
        if (length == 0) {
            return new int[0];
        }
        int shift = K % length;

        int[] shiftedArray = new int[length];

        for (int i = 0; i < length; i++) {
            shiftedArray[shift % length] = A[i];
            shift++;
        }

        return shiftedArray;
    }
}
